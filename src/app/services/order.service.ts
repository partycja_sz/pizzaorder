import { Injectable } from '@angular/core';
import { Dish } from '../models/dish.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Order } from '../models/order.model';
import { Cart } from '../models/cart.model';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  orderedDishes: Dish[] = [];
  orderPrice: number = 0;
  isOrderConfirmed: boolean = false;
  orderUrl = 'api/orders';

  constructor(readonly http: HttpClient) { }

  addToCart(dish: Dish): void {
    this.orderPrice = parseFloat((this.orderPrice + dish.price).toFixed(2));
    this.orderedDishes.push(dish);
  }

  getCart(): Dish[] {
    return this.orderedDishes;
  }

  getOrderPrice(): number {
    return this.orderPrice;
  }

  getOrderConfirmationStatus(): boolean {
    return this.isOrderConfirmed;
  }

  getOrders(): Observable<Order[]> {
    return this.http.get<Order[]>(this.orderUrl);
  }

  getOrder(id: number): Observable<Order> {
    const url = `${this.orderUrl}/${id}`;
    console.log(url);
    return this.http.get<Order>(url);
  }

  confirmOrder(order: Order): Observable<Order> {
    order.price=this.getOrderPrice();
    order.cart= this.getCart();
    this.isOrderConfirmed=true;
    return this.http.post<Order>(this.orderUrl, order, httpOptions);
  }

  resetOrder(order: Order): void {
    this.orderedDishes=[];
    this.orderPrice=0;
    this.isOrderConfirmed=false;
  }
}
