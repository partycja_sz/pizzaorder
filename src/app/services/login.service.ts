import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subscription } from 'rxjs';
import { User } from '../models/user.model';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private loginsUrl = 'api/logins';
  isLogged: boolean = false;
  subscription: Subscription;
  constructor(readonly http: HttpClient, private router: Router) {
  }

  getLogins(): Observable<User[]> {
    return this.http.get<User[]>(this.loginsUrl);
  }

  getUser(id: number): Observable<User> {
    return this.http.get<User>(`api/logins/${id}`);
  }

  login(login: string, password: string): boolean {
    let credentials: User[] = [];
    this.subscription = this.http.get<User[]>(this.loginsUrl)
      .pipe(map(result => result.filter(user => (user.username === login && user.password === password))))
      .subscribe(result => {
        credentials = result;
        if (credentials.length !== 0) {
          this.isLogged = true;
          this.router.navigate(['/admin']);
        }
      });
    return this.isLogged;
  }

  isLoggedIn(): boolean {
    return this.isLogged;
  }

  logout(): void {
    this.isLogged = false;
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
