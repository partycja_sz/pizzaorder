import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Dish } from '../models/dish.model';
import { catchError, map, tap, filter } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ItemService {

  private dishUrl = 'api/dishes';

  constructor(readonly http: HttpClient) {
  }

  getDishes(): Observable<Dish[]> {
    return this.http.get<Dish[]>(this.dishUrl);
  }

  getDishesAvailable(): Observable<Dish[]> {
    return this.http.get<Dish[]>(this.dishUrl).pipe(map(result => result.filter(dish => dish.available == true)));
  }

  getPizzasAvailable(): Observable<Dish[]> {
    return this.http.get<Dish[]>(this.dishUrl)
      .pipe(map(result => result
        .filter((dish => dish.available == true && dish.type == 'PIZZA'))));
  }

  getPastasAvailable(): Observable<Dish[]> {
    return this.http.get<Dish[]>(this.dishUrl)
      .pipe(map(result => result
        .filter((dish => dish.available == true && dish.type == 'PASTA'))));
  }

  getBevaragesAvailable(): Observable<Dish[]> {
    return this.http.get<Dish[]>(this.dishUrl)
      .pipe(map(result => result
        .filter((dish => dish.available == true && dish.type == 'BEVARAGE'))));
  }

  getDish(id: number): Observable<Dish> {
    const url = `${this.dishUrl}/${id}`;
    return this.http.get<Dish>(url).pipe(
      tap(_ => this.log(`fetched dish id=${id}`)),
      catchError(this.handleError<Dish>(`getDish id=${id}`))
    );
  }

  deleteDish(id: number): Observable<void> {
    return this.http.delete<void>(`api/dishes/${id}`);
  }
 
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  private log(message: string) {
  }

  updateDish(dish: Dish, id: number): Observable<Dish> {
    const url = `${this.dishUrl}/${id}`;
    return this.http.patch<Dish>(url, dish, httpOptions);
  }
}
