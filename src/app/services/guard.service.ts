import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root'
})
export class GuardService implements CanActivate {

  constructor(private loginService: LoginService, private router: Router) {
  }

  canActivate(): boolean {
    let isNotLogged: boolean  = !this.loginService.isLoggedIn();
    if (isNotLogged) {
      this.router.navigate(['/login']);
    }
    return true;
  }
}
