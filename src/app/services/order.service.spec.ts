import { TestBed, inject } from '@angular/core/testing';

import { OrderService } from './order.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Dish } from '../models/dish.model';
import { of } from 'rxjs';

describe('OrderService', () => {
  let httpMock;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OrderService, HttpClient, HttpHandler],
      imports: [HttpClientTestingModule, RouterTestingModule, FormsModule]
    });
    httpMock = TestBed.get(HttpTestingController);
  });

  class OrderServiceMock {
    getOrders() {
      of([{
        "price": 26,
        "date": "a",
        "time": "a",
        "cart": [
          {
            "id": 1,
            "name": "Pizza Margherita",
            "type": "PIZZA",
            "price": 10,
            "details": "Simple pizza",
            "icon": "/assets/pizza.jpeg",
            "available": true
          },
          {
            "id": 3,
            "name": "Pasta bolognese",
            "type": "PASTA",
            "price": 16,
            "details": "Simple pasta with sth",
            "icon": "/assets/pizza.jpeg",
            "available": true
          }
        ],
        "addressDelivery": {
          "city": "a",
          "street": "a",
          "buildingNo": "a",
          "flatNo": "a",
          "zipCode": "a"
        },
        "id": 1
      }
      ]);
    }
  }


  it('should be created', inject([OrderService], (service: OrderService) => {
    expect(service).toBeTruthy();
  }));

  it('should be cart with 1 item after add dish', inject([OrderService], (service: OrderService) => {

    //given
    let dishToAdd: any = [{
      id: 3,
      name: 'Pasta bolognese',
      type: 'PASTA',
      price: 16,
      details: 'Simple pasta with sth',
      icon: '/assets/pizza.jpeg',
      available: 'true'
    }];

    //when
    service.addToCart(dishToAdd);

    //then
    expect(service.orderedDishes.length).toBe(1);
  }));

  it('should be dish present in cart after selection', inject([OrderService], (service: OrderService) => {

    //given
    let dishOne: any = [{
      id: 3,
      name: 'Pasta bolognese',
      type: 'PASTA',
      price: 16,
      details: 'Simple pasta with sth',
      icon: '/assets/pizza.jpeg',
      available: 'true'
    }];
    let dishTwo: any = [{
      id: 6,
      name: 'Pasta bolognese',
      type: 'PASTA',
      price: 44,
      details: 'Simple pasta with sth',
      icon: '/assets/pizza.jpeg',
      available: 'true'
    }];

    //when
    service.addToCart(dishOne);
    service.addToCart(dishTwo);

    //then
    expect(service.getCart()).toContain(dishTwo);
    expect(service.getCart()).toContain(dishOne);
  }));


  it('should be orders returned', inject([OrderService], (service: OrderService) => {

    //given&when
    spyOn(service, "getOrders").and.returnValue(of([{ 
      "price": 26,
      "date": "a",
      "time": "a",
      "cart": [
        {
          "id": 1,
          "name": "Pizza Margherita",
          "type": "PIZZA",
          "price": 10,
          "details": "Simple pizza",
          "icon": "/assets/pizza.jpeg",
          "available": true
        },
        {
          "id": 3,
          "name": "Pasta bolognese",
          "type": "PASTA",
          "price": 16,
          "details": "Simple pasta with sth",
          "icon": "/assets/pizza.jpeg",
          "available": true
        }
      ],
      "addressDelivery": {
        "city": "a",
        "street": "a",
        "buildingNo": "a",
        "flatNo": "a",
        "zipCode": "a"
      },
      "id": 3
    }
    ]));

    //then
    expect(service).toBeTruthy();
  }));

  it("should return no orders", () => {
    httpMock.expectNone(`http://localhost:4200/order`);
  });

  it("should return no carts", () => {
    httpMock.expectNone(`http://localhost:4200/cart`);
  });



});
