import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ItemListComponent } from './components/item-list/item-list.component';
import { CartComponent } from './components/cart/cart.component';
import { LoginComponent } from './components/login/login.component';
import { ItemDetailComponent } from './components/item-detail/item-detail.component';
import { ItemComponent } from './components/item/item.component';
import { OrderComponent } from './components/order/order.component';
import { GuardService } from './services/guard.service';
import { AdminPanelComponent } from './components/admin-panel/admin-panel.component';
import { OrderSummaryComponent } from './components/order-summary/order-summary.component';
import { OrdersListComponent } from './components/orders-list/orders-list.component';
import { OrderDetailComponent } from './components/order-detail/order-detail.component';

const routes: Routes = [
  { path: '', redirectTo: '/item-list', pathMatch: 'full' },
  { path: 'item-list', component: ItemListComponent },
  { path: 'order-list', component: OrdersListComponent },
  { path: 'item-detail/:id', component: ItemDetailComponent, canActivate: [GuardService] },
  { path: 'cart', component: CartComponent },
  { path: 'order', component: OrderComponent },
  { path: 'order-detail/:id', component: OrderDetailComponent},
  { path: 'login', component: LoginComponent },
  { path: 'item', component: ItemComponent },
  { path: 'order-confirmation', component: OrderSummaryComponent },
  { path: 'admin', component: AdminPanelComponent, canActivate: [GuardService] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
