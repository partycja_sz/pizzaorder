import { AbstractControl, ValidatorFn } from "@angular/forms";

export function starts(startsWith): { [key: string]: any } {
  return (control: AbstractControl) => {
    return control.value.startsWith(startsWith) ? null : { startsWith };
  };
}

export function zipCodeValidator(nameRe: RegExp): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    const forbidden = nameRe.test(control.value);
    return forbidden ? {'zipCode': {value: control.value}} : null;
  };
}