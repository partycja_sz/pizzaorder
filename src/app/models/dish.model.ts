import { DishType } from "./dish-type.enum";

export interface Dish {
    id:number;
    name: string;
    type: DishType;
    price: number;
    details: string;
    icon: string;
    available: boolean;
}
