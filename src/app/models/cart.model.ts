import { Dish } from "./dish.model";

export interface Cart {
    id:number;
    orderedDishes: Dish[];
}