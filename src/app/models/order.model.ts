import { Time } from "@angular/common";
import { Dish } from './dish.model';

export interface Order {
    id: number;
    price: number;
    date: Date;
    time: Time;
    city: string;
    street: string;
    buildingNo: string;
    flatNo: string;
    zipCode: string;
    cart: Dish[];
}
