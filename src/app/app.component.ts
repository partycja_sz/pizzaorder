import { Component } from '@angular/core';
import { LoginService } from './services/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Pizza World!';

  constructor(private readonly loginService: LoginService) { }

  getLogInStatus(): boolean {
    return this.loginService.isLoggedIn();
  }
}

