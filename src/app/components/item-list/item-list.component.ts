import { Component, OnDestroy, OnInit, Input } from '@angular/core';
import { Dish } from '../../models/dish.model';
import { ItemService } from 'src/app/services/item.service';
import { OrderService } from 'src/app/services/order.service';
import { Subject } from 'rxjs';
import { takeUntil, filter } from 'rxjs/operators';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss']
})
export class ItemListComponent implements OnInit, OnDestroy {

  dishes: Dish[]=[];
  dish: Dish;
  private readonly destroy$ = new Subject();

  constructor(private readonly itemService: ItemService, private readonly orderService: OrderService) { }

  ngOnInit(): void {
    this.loadDishes();
  }

  loadDishes(): void {
    this.itemService.getDishesAvailable()
      .pipe(takeUntil(this.destroy$))
      .subscribe(res => this.dishes = res);
  }

  loadPizzas(): void {
    this.itemService.getPizzasAvailable()
      .pipe(takeUntil(this.destroy$))
      .subscribe(res => this.dishes = res);
  }

  loadPastas(): void {
    this.itemService.getPastasAvailable()
      .pipe(takeUntil(this.destroy$))
      .subscribe(res => this.dishes = res);
  }

  loadBevarages(): void {
    this.itemService.getBevaragesAvailable()
      .pipe(takeUntil(this.destroy$))
      .subscribe(res => this.dishes = res);
  }

  addToCart(dish: Dish): void {
    this.orderService.addToCart(dish);
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
