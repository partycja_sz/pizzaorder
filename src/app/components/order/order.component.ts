import { Component, OnInit, Input, Output } from '@angular/core';
import { Dish } from '../../models/dish.model';
import { ActivatedRoute } from '@angular/router';
import { OrderService } from 'src/app/services/order.service';
import { Order } from '../../models/order.model';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Subject } from 'rxjs';
import { Router } from "@angular/router";

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {
  orderedDishes: Dish[] = [];
  @Input() order: Order;
  orderData: FormGroup;
  isFormSubmitted: boolean = false;
  private readonly destroy$ = new Subject();

  constructor(private route: ActivatedRoute, private orderService: OrderService, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit(): void {

    this.createOrderForm();
  }

  createOrderForm(): void {
      this.orderData = this.formBuilder.group({
        'price': [],
        'date': [],
        'time': [],
        'cart': [],
        'addressDelivery': this.formBuilder.group({
          'city': ['', [Validators.required]],
          'street': ['', [Validators.required]],
          'buildingNo': ['', [Validators.required]],
          'flatNo': [],
          'zipCode': ['', [Validators.required]]
        })
      });
    } 

confirmOrder(){
  let order: Order = this.orderData.value;
  this.isFormSubmitted = true;
  this.orderService.confirmOrder(order).subscribe(order => this.order = order);
  this.router.navigate(['/order-confirmation']);
}

getCart(): void {
  this.orderService.getCart();
}

getOrderConfirmationStatus(): boolean{
  return this.orderService.getOrderConfirmationStatus();
}

ngOnDestroy(): void {
  this.destroy$.next();
  this.destroy$.complete();
}
}
