import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderComponent } from './order.component';
import { OrderService } from '../../services/order.service';
import { HttpClient } from 'selenium-webdriver/http';
import { HttpHandler, HttpHeaders, HttpParams } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('OrderComponent', () => {
  let component: OrderComponent;
  let fixture: ComponentFixture<OrderComponent>;
  
  const orderServiceStub = {
    
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrderComponent],
       providers: [OrderService, HttpClient, HttpHandler,HttpParams, HttpClientTestingModule, HttpTestingController],
      imports: [RouterTestingModule,HttpClientTestingModule,  FormsModule, ReactiveFormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  
});
