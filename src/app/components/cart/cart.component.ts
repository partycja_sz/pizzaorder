import { Component, OnInit, Input, Output } from '@angular/core';
import { Dish } from '../../models/dish.model';
import { Cart } from '../../models/cart.model';
import { OrderService } from 'src/app/services/order.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  orderedDishes: Dish[] = [];

  constructor(private route: ActivatedRoute, private orderService: OrderService) { }

  ngOnInit() {
    this.loadCart();
  }

   loadCart(): Dish[] {
    return this.orderService.getCart();
  }
}
