import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Dish } from '../../models/dish.model';
import { OrderService } from '../../services/order.service';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})

export class ItemComponent implements OnInit {
  @Input() dish: Dish;
  added = new EventEmitter<Dish>();
  constructor(private orderService: OrderService, private readonly loginService: LoginService) { }

  ngOnInit(): void {
  }

  addToCart(dish: Dish, event: Event): void {
    this.added.emit(this.dish);
    this.orderService.addToCart(dish);
    event.stopPropagation();
  }

  getLogInStatus(): boolean {
    return this.loginService.isLoggedIn();
  }
}
