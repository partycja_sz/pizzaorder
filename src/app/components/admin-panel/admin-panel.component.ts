import { Component, OnInit } from '@angular/core';
import { Dish } from '../../models/dish.model';
import { Order } from '../../models/order.model';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ItemService } from 'src/app/services/item.service';
import { OrderService } from 'src/app/services/order.service';

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.scss']
})
export class AdminPanelComponent implements OnInit {
  dishes: Dish[];
  orders: Order[];

  private readonly destroy$ = new Subject();

  constructor(private readonly itemService: ItemService, private readonly orderService: OrderService) { }

  ngOnInit(): void {
  }

  private loadDishes(): void {
    this.itemService.getDishes()
      .pipe(takeUntil(this.destroy$))
      .subscribe(res => this.dishes = res);
  }

  private loadOrders(): void {
    this.orderService.getOrders()
      .pipe(takeUntil(this.destroy$))
      .subscribe(res => this.orders = res);
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  delete(dish: Dish): void {
    this.itemService.deleteDish(dish.id)
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.loadDishes();
      });
  }
}
