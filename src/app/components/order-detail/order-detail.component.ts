import { Component, OnInit, Input } from '@angular/core';
import { Order } from '../../models/order.model';
import { OrderService } from '../../services/order.service';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Location } from '@angular/common';


@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.scss']
})
export class OrderDetailComponent implements OnInit {
  @Input() order: Order;
  orderData: FormGroup;
  orderSubscription: Subscription;

  constructor(private route: ActivatedRoute, 
    private location: Location, 
    private orderService: OrderService, 
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.createOrderForm();
    this.getOrder();
  }

  createOrderForm(): void {
    this.orderData = this.formBuilder.group({
      'price': [],
      'date': [],
      'time':[],
      'cart': [],
      'addressDelivery': this.formBuilder.group({
        'city': [],
        'street': [],
        'buildingNo': [],
        'flatNo': [],
        'zipCode': []
      })
    });
  }

  getOrder(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.orderService.getOrder(id)
      .subscribe(order => {
        this.order = order;
        this.orderData.setValue(this.order);
        console.log(this.orderData);
      });
  }

  goBack(): void {
    this.location.back();
  }
}
