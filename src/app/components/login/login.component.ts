import { Component, OnInit, Output, Input } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  login: string;
  password: string;
  isLoginError: boolean = false;
  @Input() isLogged: boolean = false;

  constructor(private loginService: LoginService, private router: Router) { }

  ngOnInit() {
  }

  onLoginEventHandler(): void {
    this.isLogged = this.loginService.login(this.login, this.password);
  }

  onLogoutEventHandler(): void {
    this.loginService.logout();
    this.router.navigate(['/item-list']);
  }

  getLogInStatus(): boolean {
    return this.loginService.isLoggedIn();
  }
}
