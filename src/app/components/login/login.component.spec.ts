import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { LoginService } from '../../services/login.service';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpHandler, HttpClient } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let loginService: LoginService;
  const loginStatus: boolean = false;
  const loginServiceMock = {
    isLoggedIn: jasmine.createSpy('getLogInStatus').and.returnValue(of(loginStatus)),
    login: jasmine.createSpy('login').and.returnValue(of(true)),
    logout: jasmine.createSpy('logout')
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginComponent],
      providers: [{ provide: LoginService, useValue: loginServiceMock }],
      imports: [RouterTestingModule, HttpClientTestingModule, FormsModule]
    })
      .compileComponents();
    loginService = TestBed.get(LoginService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should user have not logged in status when not logged in', () => {
    //given&when
    component.getLogInStatus();

    //then
    expect(loginService.isLoggedIn).toHaveBeenCalled();
    expect(component.getLogInStatus()).toBeTruthy();
  });

  it('should login service method been called when login', () => {
    //given
    let login = 'admin';
    let password = 'admin';

    //when
    component.onLoginEventHandler();

    //then
    expect(loginService.login).toHaveBeenCalled();
  });

  it('should login service method been called when logout', () => {
    //given&when
    component.onLogoutEventHandler();

    //then
    expect(loginService.logout).toHaveBeenCalled();
  });


});
