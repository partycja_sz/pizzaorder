import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OrderService } from '../../services/order.service';
import { Order } from '../../models/order.model';

@Component({
  selector: 'app-order-summary',
  templateUrl: './order-summary.component.html',
  styleUrls: ['./order-summary.component.scss']
})

export class OrderSummaryComponent implements OnInit {
  @Input() order: Order;

  constructor(private route: ActivatedRoute, private orderService: OrderService) { }

  ngOnInit(): void {
  }

  getCart(): void {
    this.orderService.getCart();
  }

  resetOrder(order: Order):void{
    this.orderService.resetOrder(order);
  }
}
