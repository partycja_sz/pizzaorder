import { Component, OnInit, Input } from '@angular/core';
import { Dish } from '../../models/dish.model';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ItemService } from 'src/app/services/item.service';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-item-detail',
  templateUrl: './item-detail.component.html',
  styleUrls: ['./item-detail.component.scss']
})
export class ItemDetailComponent implements OnInit {

  @Input() dish: Dish;
  dishData: FormGroup;
  dishSubscription: Subscription;

  constructor(private route: ActivatedRoute,
    private itemService: ItemService,
    private location: Location,
    private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.createDishForm();
    this.getDish();
  }

  createDishForm(): void {
    this.dishData = this.formBuilder.group({
      'id': [],
      'name': [],
      'type': [],
      'price': [],
      'details': [],
      'icon': [],
      'available': []
    });
  }

  getDish(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.itemService.getDish(id)
      .subscribe(dish => {
      this.dish = dish;
        this.dishData.setValue(this.dish);
      });
  }

  goBack(): void {
    this.location.back();
  }

  updateDish(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.dishData.patchValue({available:(this.dishData.get('available').value)});
    this.dish.available = this.dishData.get('available').value; 
    this.itemService.updateDish(this.dish, id).subscribe(dish => this.dish = dish);
  }

  ngOnDestroy(): void {
    this.dishSubscription.unsubscribe();
  }

}
