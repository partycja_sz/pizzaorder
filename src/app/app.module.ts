import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { OrderComponent } from './components/order/order.component';
import { ItemComponent } from './components/item/item.component';
import { ItemDetailComponent } from './components/item-detail/item-detail.component';
import { ItemListComponent } from './components/item-list/item-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CartComponent } from './components/cart/cart.component';
import { AdminPanelComponent } from './components/admin-panel/admin-panel.component';
import { OrdersListComponent } from './components/orders-list/orders-list.component';
import { OrderService } from 'src/app/services/order.service';
import { OrderSummaryComponent } from './components/order-summary/order-summary.component';
import { LoginService } from './services/login.service';
import { ItemService } from './services/item.service';
import { GuardService } from './services/guard.service';
import { OrderDetailComponent } from './components/order-detail/order-detail.component';
import { OrderItemComponent } from './components/order-item/order-item.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    OrderComponent,
    ItemComponent,
    ItemDetailComponent,
    ItemListComponent,
    CartComponent,
    AdminPanelComponent,
    OrdersListComponent,
    OrderSummaryComponent,
    OrderDetailComponent,
    OrderItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule, 
    ReactiveFormsModule
  ],
  providers: [OrderService, LoginService, ItemService, GuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
